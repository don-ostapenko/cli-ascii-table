<?php

try {
    unset($argv[0]);

    require __DIR__ . '/../vendor/autoload.php';
    $className = '\\App\\Cli\\' . array_shift($argv);

    if (!class_exists($className)) {
        throw new \App\Exceptions\CliException(sprintf('Class "%s" not found', $className));
    }

    $class = new $className();
    $class->execute();
} catch (\App\Exceptions\CliException $e) {
    echo 'Error: ' . $e->getMessage() . PHP_EOL;
}