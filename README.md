## About App

This is a "CLI ASCII table maker".

Steps of deployment

- Run `docker-compose up -d` in root project.
- Run `docker-compose exec php bash`.
- Run `composer install` in a container.

Array for render exist by "src/data.php" way. 

For start rendering

- Run `docker-compose exec php bash` if you aren't in there.
- Run `php bin/cli.php RenderTable` and enjoy.

~~~
 ======================
 | id |    name | ver |
 ----------------------
 |  1 | Laravel |   7 |
 |  2 | Symfony |   5 |
 |  3 |  Drupal |   8 |
 ======================
~~~