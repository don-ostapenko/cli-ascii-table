<?php

namespace App\Cli;

use App\Services\AsciiTable\SimpleAsciiTable;

class RenderTable
{

    /**
     * @var \App\Services\AsciiTable\SimpleAsciiTable
     */
    protected $builder;

    public function __construct()
    {
        $this->builder = new SimpleAsciiTable();
    }

    public function execute()
    {
        $data = include __DIR__ . '/../data.php';
        echo $this->builder->setData($data)->renderTable() . PHP_EOL;
    }

}