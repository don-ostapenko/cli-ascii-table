<?php

return [
  [
    'Quantity' => 1,
    'Product name' => 'Intel CPU Intel CPU',
    'Price' => 700.00,
    'Order no' => 'A0001',
  ],
  [
    'Order no' => 'A0002',
    'Price' => 800.00,
    'Quantity' => 15,
  ],
  [
    'Many' => 2,
    'Many1' => 1,
  ],
  [
    'Product name' => 'Intel CPU just best chip',
    'Order no' => 'B0012',
    'Price' => 10.00,
    'Quantity' => 4,
  ],
];