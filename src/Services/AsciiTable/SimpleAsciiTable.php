<?php

namespace App\Services\AsciiTable;

class SimpleAsciiTable implements SimpleAsciiTableInterface
{

    use MethodsBag;

    /**
     * @var array
     */
    protected $data;

    /**
     * @var array
     */
    protected $tableHeaderItems = [];

    /**
     * @var array
     */
    protected $columnsWidth = [];

    /**
     * @var array
     */
    protected $result = [];

    /**
     * @var string
     */
    private $charset = 'UTF-8';

    /**
     * @param  array  $data
     *
     * @return \App\Services\AsciiTable\SimpleAsciiTableInterface
     */
    public function setData(array $data): SimpleAsciiTableInterface
    {
        $this->data = $data;
        return $this;
    }

    /**
     * Base method that render table by array.
     *
     * @return string|void
     */
    public function renderTable()
    {
        if (empty($this->data)) {
            return 'Data for table not received';
        }

        $this->getUniqueKeys();
        $this->calcColumnsWidth();
        $this->renderHeader();
        $this->renderBody();
        $this->lineSeparator('=');

        return str_replace('||', '|', implode(PHP_EOL, $this->result));
    }

}