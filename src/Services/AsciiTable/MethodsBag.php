<?php

namespace App\Services\AsciiTable;

trait MethodsBag
{

    /**
     * Parse unique keys for table header.
     */
    private function getUniqueKeys()
    {
        $keys = [];

        foreach ($this->data as $row) {
            $tmpKeys = array_keys($row);
            foreach ($tmpKeys as $item) {
                array_push($keys, $item);
            }
        }

        sort($keys);
        $this->tableHeaderItems = array_unique($keys);
    }

    /**
     * Calculate the count of header items symbols.
     */
    private function calcColumnsWidth()
    {
        foreach ($this->data as $row) {
            foreach ($this->tableHeaderItems as $headerItem) {
                $this->columnsWidth[$headerItem] = max(
                  isset($this->columnsWidth[$headerItem]) ? $this->columnsWidth[$headerItem] : 0,
                  $this->length($row[$headerItem]),
                  $this->length($headerItem)
                );
            }
        }
    }

    /**
     * Calculates length for string.
     *
     * @param $str
     *
     * @return int
     */
    private function length($str)
    {
        return mb_strlen($str, $this->charset);
    }

    /**
     * Method for render header of table.
     *
     * @return void
     */
    private function renderHeader()
    {
        $this->lineSeparator('=');

        $tmp = '';

        foreach ($this->tableHeaderItems as $headerItem) {
            $tmp .= $this->column($headerItem, $headerItem);
        }

        $this->result[] = $tmp;

        $this->lineSeparator('-');
    }

    /**
     * Method for render body of table.
     *
     * @return void
     */
    private function renderBody()
    {
        foreach ($this->data as $row) {

            $tmp = '';

            foreach ($this->tableHeaderItems as $column) {
                $tmp .= $this->column($column, $row[$column]);
            }

            $this->result[] = $tmp;
        }
    }

    /**
     * Append a line separator to result.
     *
     * @param  string  $item
     */
    private function lineSeparator(string $item)
    {
        $tmp = '';

        foreach ($this->tableHeaderItems as $headerItem) {
            $tmp .= $item.str_repeat($item,
                $this->columnsWidth[$headerItem] + 1).$item;
        }

        $this->result[] = $tmp;
    }

    /**
     * Generate string row of body for result.
     *
     * @param $headerItemKey
     * @param $value
     *
     * @return string
     */
    private function column($headerItemKey, $value)
    {
        return '| '.str_repeat(' ',
            $this->columnsWidth[$headerItemKey] - $this->length($value)).$value.' '.'|';
    }

}