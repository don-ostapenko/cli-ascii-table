<?php

namespace App\Services\AsciiTable;

interface SimpleAsciiTableInterface
{
    public function setData(array $data): self;
}