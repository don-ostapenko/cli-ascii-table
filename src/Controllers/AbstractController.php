<?php

namespace App\Controllers;

use App\Exceptions\ServiceNotFoundException;
use App\Services\Di\DiContainer;
use App\Services\Http\Request;
use App\View\View;

abstract class AbstractController
{

    /**
     * @var View
     */
    protected $view;

    /**
     * @var Request
     */
    protected $request;

    /**
     * AbstractController constructor.
     *
     * @param  DiContainer  $di
     *
     * @throws ServiceNotFoundException
     */
    public function __construct($di)
    {
        $this->request = $di->get(Request::class);
        $this->view = $di->get(View::class);
    }

}